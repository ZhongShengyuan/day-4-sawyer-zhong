package com.afs.tdd;

import java.util.List;

public class MarsRover {

    private final Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command.equals(Command.Move)) {
            move();
        }
        if (command.equals(Command.Left)) {
            turnleft();
        }
        if (command.equals(Command.Right)) {
            turnRight();
        }
    }

    public void move() {
        if (location.getDirection().equals(Direction.North)) {
            location.setCoordinateY(location.getCoordinateY() + 1);
        } else if (location.getDirection().equals(Direction.East)) {
            location.setCoordinateX(location.getCoordinateX() + 1);
        } else if (location.getDirection().equals(Direction.West)) {
            location.setCoordinateX(location.getCoordinateX() - 1);
        } else if (location.getDirection().equals(Direction.South)) {
            location.setCoordinateY(location.getCoordinateY() - 1);
        }
    }

/*    private void turnleft() {
        if (location.getDirection().equals(Direction.North)) {
            location.setDirection(Direction.West);
        } else if (location.getDirection().equals(Direction.West)) {
            location.setDirection(Direction.South);
        } else if (location.getDirection().equals(Direction.South)) {
            location.setDirection(Direction.East);
        } else {
            location.setDirection(Direction.North);
        }
    }

    private void turnRight() {
        if (location.getDirection().equals(Direction.North)) {
            location.setDirection(Direction.East);
        } else if (location.getDirection().equals(Direction.West)) {
            location.setDirection(Direction.North);
        } else if (location.getDirection().equals(Direction.South)) {
            location.setDirection(Direction.West);
        } else if (location.getDirection().equals(Direction.East)) {
            location.setDirection(Direction.South);
        }
    }*/

    private void turnleft() {
        int index = (location.getDirection().getIndex() + 1) % 4;
        location.setDirection(Direction.getDirectionByIndex(index));
    }

    private void turnRight() {
        int index = (location.getDirection().getIndex() + 3) % 4;
        location.setDirection(Direction.getDirectionByIndex(index));
    }

    public void executeCommands(List<Command> commands){
        commands.stream().forEach(command -> {
            if(command == Command.Move)
                move();
            if(command == Command.Left)
                turnleft();
            if(command == Command.Right)
                turnRight();
        });
    }

    public Location getLocation() {
        return location;
    }
}
