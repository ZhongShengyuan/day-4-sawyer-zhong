package com.afs.tdd;

import java.util.Arrays;

public enum Direction {
    West(1), North(0),
    East(3), South(2);

    private int index;

    Direction(int index) {
        this.index = index;
    }

    public static Direction getDirectionByIndex(int index) {
        return Arrays.stream(Direction.values())
                .filter(item -> item.index == index)
                .findAny().orElse(null);
    }

    public int getIndex() {
        return index;
    }
}
